// SFETCU IULIAN-ANDREI 333CB

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <complex.h>

typedef double complex cplx;

pthread_barrier_t barrier;

int N;
int P;
cplx *input_numbers;
cplx *out;
double re;

void FFT_recursion(cplx buf[], cplx out[], int n, int step) {
	
	if (step < n) {
		FFT_recursion(out, buf, n, step * 2);
		FFT_recursion(out + step, buf + step, n, step * 2);
 
		for (int i = 0; i < n; i += 2 * step) {
			cplx t = cexp(-I * M_PI * i / n) * out[i + step];
			buf[i / 2]     = out[i] + t;
			buf[(i + n)/2] = out[i] - t;
		}
	}
}

void* threadFunction(void *var) {

	int thread_id = *(int*)var;

	// One thread case
	if (P == 1) {
		FFT_recursion(input_numbers, out, N, 1);
	}

	// Two threads case
	else if (P == 2) {
		if(thread_id == 0) {
			FFT_recursion(out, input_numbers, N, 2);
			pthread_barrier_wait(&barrier);
		}
		else if (thread_id == 1) {
			FFT_recursion(out + 1, input_numbers + 1, N, 2);
			pthread_barrier_wait(&barrier);
			for (int i = 0; i < N; i += 2) {
				cplx t = cexp(-I * M_PI * i / N) * out[i + 1];
				input_numbers[i / 2] = out[i] + t;
				input_numbers[(i + N) / 2] = out[i] - t;
			}	
		}
	}

	// Four threads case
	else if (P == 4) {
		if(thread_id == 0) {
			FFT_recursion(input_numbers, out, N, 4);
			pthread_barrier_wait(&barrier);
		}
		else if (thread_id == 1) {
			FFT_recursion(input_numbers + 1, out + 1, N, 4);
			pthread_barrier_wait(&barrier);
		}
		else if (thread_id == 2) {
			FFT_recursion(input_numbers + 2, out + 2, N, 4);
			pthread_barrier_wait(&barrier);
		}
		else if (thread_id == 3) {
			FFT_recursion(input_numbers + 3, out + 3, N, 4);
			pthread_barrier_wait(&barrier);
			for (int i = 0; i < N; i += 4) {
				cplx t = cexp(-I * M_PI * i / N) * input_numbers[i + 2];
				out[i / 2] = input_numbers[i] + t;
				out[(i + N) / 2] = input_numbers[i] - t;
			}
			for (int i = 0; i < N; i += 4) {
				cplx t = cexp(-I * M_PI * i / N) * input_numbers[i + 3];
				out[i / 2 + 1] = input_numbers[i + 1] + t;
				out[(i + N) / 2 + 1] = input_numbers[i + 1] - t;
			}
			for (int i = 0; i < N; i += 2) {
				cplx t = cexp(-I * M_PI * i / N) * out[i + 1];
				input_numbers[i / 2] = out[i] + t;
				input_numbers[(i + N) / 2] = out[i] - t;
			}	
		}
	}
}

int main(int argc, char * argv[]) {

	int reading = -1, writing = -1, i;
	pthread_t tid[P];
	int thread_id[P];
	char *n;

	P = strtol(argv[3], &n, 10);

	pthread_barrier_init(&barrier, NULL, P);

	FILE *input = fopen(argv[1], "r");
	FILE *output = fopen(argv[2], "w");

	if(!input) {
	   printf("READING [ERROR]: Cannot open file!");
	   exit(-1);
	}

	if(!output) {
	   printf("WRITING [ERROR]: Cannot open file!");
	   exit(-1);
	}

	// N reading and memory allocation
	reading = fscanf(input, "%d", &N);
	input_numbers = (complex*) malloc(N * sizeof(complex));
	out = (complex*) malloc(N * sizeof(complex));

    for(i = 0; i < N; i++) {
        reading = fscanf(input,"%lf", &re);
		complex c = re + 0 * I;
		input_numbers[i] = c;
		out[i] = c;
    }

    if (reading < 0) {
   		printf("WRITING [ERROR]: Cannot read from file!");
	   	exit(-1);
    }

    // Run threads
	for(i = 0; i < P; i++) {
		thread_id[i] = i;
	}

	for(i = 0; i < P; i++) {
		pthread_create(&(tid[i]), NULL, threadFunction, &(thread_id[i]));
	}

	for(i = 0; i < P; i++) {
		pthread_join(tid[i], NULL);
	}

    // Values writing
    fprintf(output, "%d\n", N);

    for(int i = 0; i < N; i++) {
        writing = fprintf(output, "%lf %lf\n", creal(input_numbers[i]), cimag(input_numbers[i]));
    }

    if (writing < 0) {
   		printf("WRITING [ERROR]: Cannot write in file!");
	   	exit(-1);
    }

    // Barrier destroy
    pthread_barrier_destroy(&barrier);

    // Free used memory
    free(input_numbers);
    free(out);

	return 0;
}
