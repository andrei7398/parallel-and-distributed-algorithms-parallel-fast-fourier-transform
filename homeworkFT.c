// SFETCU IULIAN-ANDREI 333CB

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

int N;
double *input_numbers;
double *Re;
double *Im;
int P;

void* threadFunction(void *var) {

	int thread_id = *(int*)var;

	int start = thread_id * ceil((double) N / (double) P);
	int end = fmin(N, (thread_id + 1) * ceil((double) N / (double) P));

    // Computation
    for (int k = start ; k < end; k++) {
        // Real part
        Re[k] = 0;
        for (int i = 0 ; i < N ; i++) {
        	Re[k] += input_numbers[i] * cos(i * k * 2 * M_PI / N);
        }
         
        // Imaginary part
        Im[k] = 0;
        for (int i = 0; i < N ; i++) {
        	Im[k] -= input_numbers[i] * sin(i * k * 2 * M_PI / N);
        }
    }
}


int main(int argc, char *argv[]) {

	int reading = -1, writing = -1, i;
	pthread_t tid[P];
	int thread_id[P];
	char *n_to_convert;

	P = strtol(argv[3], &n_to_convert, 10);;

	FILE *input = fopen(argv[1], "r");
	FILE *output = fopen(argv[2], "w");

	if(!input) {
	   printf("READING [ERROR]: Cannot open file!");
	   exit(-1);
	}

	if(!output) {
	   printf("WRITING [ERROR]: Cannot open file!");
	   exit(-1);
	}


	// N reading and memory allocation
	reading = fscanf(input, "%d", &N);
	input_numbers = (double*) malloc(N * sizeof(double));
	Re = (double*) malloc(N * sizeof(double));
	Im = (double*) malloc(N * sizeof(double));


	// Values reading
    for(i = 0; i < N; i++) {
        reading = fscanf(input, "%lf", &input_numbers[i]);
    }

    if (reading < 0) {
   		printf("WRITING [ERROR]: Cannot read from file!");
	   	exit(-1);
    }


    // Run threads
	for(i = 0; i < P; i++) {
		thread_id[i] = i;
	}

	for(i = 0; i < P; i++) {
		pthread_create(&(tid[i]), NULL, threadFunction, &(thread_id[i]));
	}

	for(i = 0; i < P; i++) {
		pthread_join(tid[i], NULL);
	}

    // Values writing
    fprintf(output, "%d\n", N);

    for(int i = 0; i < N; i++) {
        writing = fprintf(output, "%lf %lf\n", Re[i], Im[i]);
    }

    if (writing < 0) {
   		printf("WRITING [ERROR]: Cannot write in file!");
	   	exit(-1);
    }

    // Free used memory
    free(input_numbers);
    free(Re);
    free(Im);

	return 0;
}
